# df_diabete_pima_indians
- Pregnancies
To express the Number of pregnancies

- Glucose
To express the Glucose level in blood

- BloodPressure
To express the Blood pressure measurement

- SkinThickness
To express the thickness of the skin

- Insulin
To express the Insulin level in blood

- BMI
To express the Body mass index

- DiabetesPedigreeFunction
To express the Diabetes percentage

- Age
To express the age

- Outcome
To express the final result 1 is YES o is NO


# df_012_health_indicators
- Diabetes_012
0 = no diabetes 1 = prediabetes 2 = diabetes

- HighBP
0 = no high BP 1 = high BP

- HighChol
0 = no high cholesterol 1 = high cholesterol

- CholCheck
0 = no cholesterol check in 5 years 1 = yes cholesterol check in 5 years

- BMI
Body Mass Index

- Smoker
Have you smoked at least 100 cigarettes in your entire life? [Note: 5 packs = 100 cigarettes] 0 = no 1 = yes

- Stroke
(Ever told) you had a stroke. 0 = no 1 = yes

- HeartDiseaseorAttack
coronary heart disease (CHD) or myocardial infarction (MI) 0 = no 1 = yes

- PhysActivity
physical activity in past 30 days - not including job 0 = no 1 = yes

- Fruits
Consume Fruit 1 or more times per day 0 = no 1 = yes 


# df_binary_split_health_indicators
- Diabetes_binary
0 = no diabetes 1 = prediabetes 2 = diabetes

- HighBP
0 = no high BP 1 = high BP

- HighChol
0 = no high cholesterol 1 = high cholesterol

- CholCheck
0 = no cholesterol check in 5 years 1 = yes cholesterol check in 5 years

- BMI
Body Mass Index

- Smoker
Have you smoked at least 100 cigarettes in your entire life? [Note: 5 packs = 100 cigarettes] 0 = no 1 = yes

- Stroke
(Ever told) you had a stroke. 0 = no 1 = yes

- HeartDiseaseorAttack
coronary heart disease (CHD) or myocardial infarction (MI) 0 = no 1 = yes

- PhysActivity
physical activity in past 30 days - not including job 0 = no 1 = yes

- Fruits
Consume Fruit 1 or more times per day 0 = no 1 = yes

# df_binary_health_indicators
- Diabetes_012
0 = no diabetes 1 = prediabetes 2 = diabetes

- HighBP
0 = no high BP 1 = high BP

- HighChol
0 = no high cholesterol 1 = high cholesterol

- CholCheck
0 = no cholesterol check in 5 years 1 = yes cholesterol check in 5 years

- BMI
Body Mass Index

- Smoker
Have you smoked at least 100 cigarettes in your entire life? [Note: 5 packs = 100 cigarettes] 0 = no 1 = yes

- Stroke
(Ever told) you had a stroke. 0 = no 1 = yes

- HeartDiseaseorAttack
coronary heart disease (CHD) or myocardial infarction (MI) 0 = no 1 = yes

- PhysActivity
physical activity in past 30 days - not including job 0 = no 1 = yes

- Fruits
Consume Fruit 1 or more times per day 0 = no 1 = yes

- Veggies
Consume Vegetables 1 or more times per day 0 = no 1 = yes

- HvyAlcoholConsump
Heavy drinkers (adult men having more than 14 drinks per week and adult women having more than 7 drinks per week) 0 = no 1 = yes

- AnyHealthcare
Have any kind of health care coverage, including health insurance, prepaid plans such as HMO, etc. 0 = no 1 = yes

- NoDocbcCost
Was there a time in the past 12 months when you needed to see a doctor but could not because of cost? 0 = no 1 = yes

- GenHlth
Would you say that in general your health is: scale 1-5 1 = excellent 2 = very good 3 = good 4 = fair 5 = poor

- MentHlth
Now thinking about your mental health, which includes stress, depression, and problems with emotions, for how many days during the past 30 days was your mental health not good? scale 1-30 days

- PhysHlth
Now thinking about your physical health, which includes physical illness and injury, for how many days during the past 30 days was your physical health not good? scale 1-30 days

- DiffWalk
Do you have serious difficulty walking or climbing stairs? 0 = no 1 = yes

- Sex
0 = female 1 = male

- Age
13-level age category (_AGEG5YR see codebook) 1 = 18-24 9 = 60-64 13 = 80 or older

- Education
Education level (EDUCA see codebook) scale 1-6 1 = Never attended school or only kindergarten 2 = Grades 1 through 8 (Elementary) 3 = Grades 9 through 11 (Some high school) 4 = Grade 12 or GED (High school graduate) 5 = College 1 year to 3 years (Some college or technical school) 6 = College 4 years or more (College graduate)

- Income
Income scale (INCOME2 see codebook) scale 1-8 1 = less than $10,000 5 = less than $35,000 8 = $75,000 or more 


# df_prediction
- gender: <str> --> enum 2 catégories
Gender refers to the biological sex of the individual, which can have an impact on their susceptibility to diabetes. There are three categories in it male ,female and other.

- age: <int>
Age is an important factor as diabetes is more commonly diagnosed in older adults.Age ranges from 0-80 in our dataset.

- hypertension: <boolean>
Hypertension is a medical condition in which the blood pressure in the arteries is persistently elevated. It has values a 0 or 1 where 0 indicates they don’t have hypertension and for 1 it means they have hypertension.

- heart_disease: <boolean>
Heart disease is another medical condition that is associated with an increased risk of developing diabetes. It has values a 0 or 1 where 0 indicates they don’t have heart disease and for 1 it means they have heart disease.

- smoking_history: <str> --> enum 5 catégories
Smoking history is also considered a risk factor for diabetes and can exacerbate the complications associated with diabetes.In our dataset we have 5 categories i.e not current,former,No Info,current,never and ever.

- bmi: <float>
BMI (Body Mass Index) is a measure of body fat based on weight and height. Higher BMI values are linked to a higher risk of diabetes. The range of BMI in the dataset is from 10.16 to 71.55. BMI less than 18.5 is underweight, 18.5-24.9 is normal, 25-29.9 is overweight, and 30 or more is obese.

- HbA1c_level: <float>
HbA1c (Hemoglobin A1c) level is a measure of a person's average blood sugar level over the past 2-3 months. Higher levels indicate a greater risk of developing diabetes. Mostly more than 6.5% of HbA1c Level indicates diabetes.

- blood_glucose_level: <int>
Blood glucose level refers to the amount of glucose in the bloodstream at a given time. High blood glucose levels are a key indicator of diabetes.

- diabetes: <boolean>
Diabetes is the target variable being predicted, with values of 1 indicating the presence of diabetes and 0 indicating the absence of diabetes.


# Autres df

la pression systolique (PAS) est la pression maximale, au moment de la « contraction » du cœur (systole)
--> bp.1s
la pression diastolique (PAD) est la pression minimale, au moment du « relâchement » du cœur (diastole)
--> bp.1d

"HDL" signifie "lipoprotéine de haute densité". Le HDL est souvent qualifié de "bon cholestérol"
"LDL" signifie "lipoprotéine de basse densité". Le HDL est souvent qualifié de "mauvais cholestérol"
Le cholestérol total est la quantité totale de cholestérol qui circule dans le sang. 
Il est calculé par la formule suivante : HDL + LDL + 20 % de triglycérides = cholestérol total. 

- stab.glu = glucose
- age
- ratio = chol / hdl ou inverse
- chol = cholestérol
- waist circonférence taille
- bp.1s presion arterielle systolique first mesure
- weight poids
- hip hanche taille
- bp.2s presion arterielle systolique second mesure